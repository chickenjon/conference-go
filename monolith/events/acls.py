from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_pexel_image(city, state):

    url = "https://api.pexels.com/v1/search"
    params = {
        "query": city + " " + state,
        "per_page": 1,
    }
    headers = {"Authorization": PEXELS_API_KEY}
    r = requests.get(url, params=params, headers=headers)
    photo_data = json.loads(r.content)

    try:
        return photo_data["photos"][0]["src"]["large"]
    except (KeyError, IndexError):
        return None


def get_weather_data(city, state):

    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": city + "," + state,
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    r = requests.get(url, params=params)
    location_data = json.loads(r.content)

    lat = location_data[0]["lat"]
    lon = location_data[0]["lon"]

    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }
    r = requests.get(url, params=params)
    weather_data = json.loads(r.content)

    try:
        temp = weather_data["main"]["temp"]
        desc = weather_data["weather"][0]["description"]

        return {
            "description": desc,
            "temp": temp,
        }
    except (KeyError, IndexError):
        return None
